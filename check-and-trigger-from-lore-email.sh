#!/bin/bash
#

KERNEL_URL=${KERNEL_URL:-"https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git"}

LAVA_TEST_PLANS_PROJECT=${LAVA_TEST_PLANS_PROJECT:-"lkft"}
QA_TEAM=${QA_TEAM:-"lkft"}
LINUX_KERNEL=${LINUX_KERNEL:-"linux-stable-rc"}

old_sha=$(cat latest-checked-sha.txt | awk -F ' ' '{print $1}')
git log --oneline  HEAD...${old_sha} | grep '\w \[PATCH .* review$'|tee kernel-rc-reviews

for release in $(cat kernel-rc-reviews| awk -F ' ' '{print $3}'); do
	echo ${release}
	git ls-remote ${KERNEL_URL} linux-${release}.y | tee kernel-ls-remote
	branch=$(cat kernel-ls-remote | awk -F '/' '{print $NF}')
	sha=$(cat kernel-ls-remote | awk -F ' ' '{print $1}')
	qa_project="${LINUX_KERNEL}-${branch}"
	./squad-client-cup.sh ${qa_project} "${LINUX_KERNEL} ${branch}"
	#tuxconfig="https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/tuxconfig/${branch}.yml"
	tuxconfig="https://gitlab.com/aroxell/lore-pipelines/raw/main/${branch}.yml"
	tuxsuite build --git-repo ${KERNEL_URL} --git-sha ${sha} --target-arch arm64 --toolchain gcc-13 --kconfig tinyconfig --show-logs --json-out build.json config
	git_describe=$(jq -r '.git_describe' build.json)
	callback="--callback ${QA_SERVER}/api/fetchjob/${QA_TEAM}/${qa_project}/${git_describe}/env/tuxsuite.com";
	tuxsuite plan --git-repo ${KERNEL_URL} --git-sha ${sha} "${tuxconfig}" --json-out build-plan.json --no-wait ${callback} --lava-test-plans-project ${LAVA_TEST_PLANS_PROJECT}

done
